import subprocess
import platform

dns_options = [
    ("Google Public DNS", "8.8.8.8, 8.8.4.4", "Fast and reliable public DNS"),
    ("Cloudflare DNS", "1.1.1.1, 1.0.0.1", "Privacy-focused public DNS"),
    ("OpenDNS", "208.67.222.222, 208.67.220.220", "Family-friendly filtering options"),
    ("dns-crypt", "https://dnscrypt.info/resolvers/", "Encrypted and decentralized DNS"),
    ("Tor DNS", "9.9.9.9, 149.117.200.129", "Anonymizing DNS through Tor relays"),
    ("Mullvad DNS", "104.208.50.46, 2001:41d0:0e0e::1", "Privacy-focused DNS by VPN provider"),
    ("Free DNS", "31.134.41.95, 213.248.76.68", "Fast and reliable with global coverage"),
    ("NordVPN DNS", "103.54.251.49, 103.54.252.49", "Secure and private DNS from NordVPN"),
    ("Level3 DNS", "4.2.2.1, 4.2.2.2", "Secure and reliable DNS from Level3 Communications"),
    ("Yandex DNS", "8.8.8.8, 8.8.4.4", "Fast and reliable Russian DNS with security features"),
    ("Quad9 DNS", "9.9.9.9, 149.117.200.129", "Focused on security and blocking phishing websites"),
    ("AdGuard DNS", "172.67.196.221, 172.67.197.221", "DNS with ad blocking and malware protection"),
]

def get_current_dns():
    """Retrieves the current DNS servers."""
    os_name = platform.system()
    if os_name == "Windows":
        output = subprocess.check_output(["ipconfig", "/all"]).decode()
        dns_servers = [line.split(":")[1].strip() for line in output.splitlines()
                       if "DNS Servers" in line]
    elif os_name == "Linux" or os_name == "Darwin":
        output = subprocess.check_output(["cat", "/etc/resolv.conf"]).decode()
        dns_servers = [line.split("nameserver")[1].strip() for line in output.splitlines()
                       if "nameserver" in line]
    else:
        raise RuntimeError("Unsupported operating system")
    return dns_servers

def list_dns_options():
    """Presents a list of DNS options with descriptions."""
    for i, (name, servers, description) in enumerate(dns_options):
        print(f"{i+1}. {name} ({servers}) - {description}")

def set_dns(servers):
    """Sets the DNS servers."""
    os_name = platform.system()
    if os_name == "Windows":
        # Use Windows-specific methods to set DNS
        # (e.g., netsh or registry changes)
        raise NotImplementedError("DNS setting for Windows is not implemented yet")
    elif os_name == "Linux" or os_name == "Darwin":
        with open("/etc/resolv.conf", "w") as f:
            f.write("nameserver {}".format(servers[0]))
            if len(servers) > 1:
                f.write("\nnameserver {}".format(servers[1]))
    else:
        raise RuntimeError("Unsupported operating system")

def main():
    current_dns = get_current_dns()
    print("Current DNS servers:", current_dns)

    list_dns_options()
    choice = input("Enter the number of the DNS option you want to use, or 0 to keep the current one: ")

    if choice == "0":
        print("Keeping current DNS servers.")
    else:
        try:
            choice = int(choice) - 1
            new_servers = dns_options[choice][1].split(", ")
            set_dns(new_servers)
            print("DNS servers updated successfully!")
        except (IndexError, ValueError):
            print("Invalid choice. Please enter a valid number.")

if __name__ == "__main__":
    main()
